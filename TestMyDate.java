/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class TestMyDate {
    public static void main(String args[]) {
        System.out.println("////DAY/////");
        MyDate d1 = new MyDate (2000,12,31);
        System.out.println("CUR :" + d1);
        System.out.println("NEXT:" + d1.nextDay());
        System.out.println("PREV: " +d1.previousDay());
        System.out.println("////MONTH/////");
        MyDate d2 = new MyDate (2012,10,31);
        System.out.println("CUR :" + d2);
        System.out.println("NEXT:" + d2.nextMonth());
        System.out.println("PREV: " + d2.previousMonth());
        System.out.println("////YEAR/////");
        MyDate d3 = new MyDate (2012,02,29);
        System.out.println("CUR :" + d3);
        System.out.println("NEXT:" + d3.nextYear());
        System.out.println("NEXT:" + d3.previousYear());
        System.out.println("12-8-2011 to 3-2-2012");
        System.out.println("////LOOP-NEXT/////");
        MyDate d4 = new MyDate (2011,12,28);
        for(int i=1; i< 67; i++) {
            System.out.println(d4.nextDay());
        }        
        
    }    
}
