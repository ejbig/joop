/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author biglangawa
 */
package mypoint;
import java.io.*;
import java.util.*;

// fileName ="C:\\Users\\biglangawa\\Training1\\MyPoint\\src\\mypoint\\polynomial.txt"; 
public class MyPolynomial {
    private double[] coeffs;
    private int d;
    
    public MyPolynomial(double... coeffs) {
        this.coeffs = coeffs;
        this.d = getDegree();
    }
    
    
    public MyPolynomial(String filename) {
        Scanner in = null;
        try {
            in = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int degree = in.nextInt();
        coeffs = new double[degree+1];
        for(int i=0; i<coeffs.length; i++) {
            coeffs[i] = in.nextDouble();
        }
    }
    
    public int getDegree() {
        int d = 0;
        for (int i = 0; i < coeffs.length; i++) {
            if (coeffs[i] !=0) d = i;
        }
        return d;
    }
    
    public double evaluate(double x) {
        double result = 0;
        int d = getDegree();
        for(int i = d; i >=0; i--) {
            result = coeffs[i] + (x * result);
        }
        return result;
    }
    
    public String toString() {
        int d = getDegree();
        if(d==0) return "" + coeffs[0];
        if(d==1) return "" + coeffs[1] + "x " + coeffs[0];
        String pDeg = coeffs[d] + "x^" + d;
        for (int i = d-1; i >= 0; i--) {
            if      (coeffs[i] == 0) continue;
            else if (coeffs[i]  > 0) pDeg = pDeg + " + " + ( coeffs[i]);
            else if (coeffs[i]  < 0) pDeg = pDeg + " - " + (-coeffs[i]);
            if      (i == 1) pDeg = pDeg + "x";
            else if (i >  1) pDeg = pDeg + "x^" + i;
        }
        return pDeg;
    }
    
    public MyPolynomial add (MyPolynomial another) {
        int d1 = this.getDegree();
        int d2 = another.getDegree();
        int deg = 0;
        if(d1 > d2) {
            deg = d1;
        } else {
            deg = d2;  
        }
        double[] c1 = new double[deg+1];
        for (int i=0; i<=deg; i++) {
            c1[i] = this.coeffs[i] + another.coeffs[i];
        }

        return  new MyPolynomial(c1);
    }
    
public MyPolynomial multiply (MyPolynomial another) {
        int d1 = this.getDegree();
        int d2 = another.getDegree();
        int deg = 0;
        if(d1 > d2) {
            deg = d1;
        } else {
            deg = d2;  
        }
        double[] c1 = new double[deg+1];
        for (int i=0; i<=deg; i++) {
            c1[i] = this.coeffs[i] * another.coeffs[i];
        }

        return  new MyPolynomial(c1);
    }
    
}
