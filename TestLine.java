/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class TestLine {
    public static void main(String[] args) {
        System.out.println("////////COMPOSITION////////");
        Line l1 = new Line(0, 0, 3, 4);
        System.out.println(l1);
        
        MyPoint p1 = new MyPoint(5,6);
        MyPoint p2 = new MyPoint(9,10);
        Line l2 = new Line(p1,p2);
        System.out.println(l2);
        
        System.out.println("////////INHERITED/////////");
        LineSub l3 = new LineSub(0, 0, 3, 4);
        System.out.println(l3);
        
        MyPoint p3 = new MyPoint(5,6);
        MyPoint p4 = new MyPoint(9,10);
        LineSub l4 = new LineSub(p3,p4);
        System.out.println(l4);
    }
    
}
