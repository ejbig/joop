/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class LineSub extends MyPoint {
    MyPoint end;
    
    public LineSub(int beginX, int beginY, int endX, int endY) {
        super(beginX, beginY);
        this.end = new MyPoint(endX, endY);
    }
    
    public LineSub(MyPoint begin, MyPoint end) {
        super(begin.getX(), begin.getY());
        this.end = end;
    }
    
    @Override
    public String toString() {
        return "Begin: (" + this.x + "," + this.y + ") End: " + this.end; 
    }
    
    public int getBeginX() {
        return this.x;
    }
    
    public int getBeginY() {
        return this.y;
    }
    
    public int getEndX() {
        return this.end.x;
    }
    
    public int getEndY() {
        return this.end.y;
    }
    
    public void setBeginX(int x) {
        this.x = x;
    }
    
    public void setBeginY(int y) {
        this.y = y;
    }
    
    public void setBeginXY(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void setEndX(int x) {
        this.end.x = x;
    }
    
    public void setEndY(int y) {
        this.end.y = y;
    }
    
    public void setEndXY(int x, int y) {
        this.end.x = x;
        this.end.y = y;
    }
    
    public double getLength() {
        int xDiff = this.x - this.end.x;
        int yDiff = this.y - this.end.y;
        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
    }
    
    public double getGradient() {
        int xDiff = this.x - this.end.x;
        int yDiff = this.y - this.end.y;
        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
    }
    
    
    
    
}
