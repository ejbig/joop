/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class Line {
    private MyPoint begin;
    private MyPoint end;
    
    public Line(MyPoint begin, MyPoint end) {
        this.begin = begin;
        this.end = end;
    }
    
    public Line(int beginX, int beginY, int endX, int endY) {
        begin = new MyPoint(beginX, beginY);
        end = new MyPoint(endX, endY);
    }
    
    public String toString() {
        return "Begin: " + this.begin + " End: " + this.end; 
    }
    
    public int getBeginX() {
        return this.begin.x;
    }
    
    public int getBeginY() {
        return this.begin.y;
    }
    
    public int getEndX() {
        return this.end.x;
    }
    
    public int getEndY() {
        return this.end.y;
    }
    
    public void setBeginX(int x) {
        this.begin.x = x;
    }
    
    public void setBeginY(int y) {
        this.begin.y = y;
    }
    
    public void setBeginXY(int x, int y) {
        this.begin.x = x;
        this.begin.y = y;
    }
    
    public void setEndX(int x) {
        this.end.x = x;
    }
    
    public void setEndY(int y) {
        this.end.y = y;
    }
    
    public void setEndXY(int x, int y) {
        this.end.x = x;
        this.end.y = y;
    }
    
    public double getLength() {
        int xDiff = this.begin.x - this.end.x;
        int yDiff = this.begin.y - this.end.y;
        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
    }
    
    public double getGradient() {
        int xDiff = this.begin.x - this.end.x;
        int yDiff = this.begin.y - this.end.y;
        return Math.sqrt(xDiff*xDiff + yDiff*yDiff);
    }
    
}
