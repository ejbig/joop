/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package e212;


/**
 *
 * @author biglangawa

 */
public class Circle extends Shape	{
	
	private double radius;

	public	Circle()	{
            radius = 1.0;
	}
        
        public Circle(double radius)	{
            this.radius = radius;
	}


	public Circle(double radius, String color, boolean filled)	{
            super(color,filled);
            this.radius = radius;
	}


public class Circle	{
	
	private double radius;


	public	Circle()	{
            radius = 1.0;
            color = "red";
	}


	public Circle(double radius, String color)	{
            this.radius = radius;
            this.color = color;
	}


	public double getRadius()	{
            return radius;	
	}

	public double getArea()	{
            return radius*radius*Math.PI;
	}
        
        public void setRadius(double radius) {
            this.radius = radius;
        }
        

        public double getPerimeter() {
            return 2*(Math.PI) * radius;
        }
        
        @Override
        public String toString() {
            return "A Circle with radius=" + radius + " which is a subclass of " + super.toString();
	}

        public void setColor(String color) {
            this.color = color;
        }
            
        
	
}
