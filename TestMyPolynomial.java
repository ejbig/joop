/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class TestMyPolynomial {
    public static void main(String[] args) {
        MyPolynomial p1 = new MyPolynomial(1.1,2.2,3.3,4.4);
        System.out.println(p1);
        double coeffs[] = {1,2,4,6};
        MyPolynomial p2 = new MyPolynomial(coeffs);
        System.out.println(p2);
        String filePoly = "C:\\Users\\biglangawa\\Training1\\MyPoint\\src\\mypoint\\polynomial.txt";
        MyPolynomial p3 = new MyPolynomial(filePoly);
        System.out.println(p3);
        
        System.out.println("Evaluation" + p3.evaluate(-3));
        System.out.println("/////ADDITION////");
        System.out.println("+ "+p2);
        System.out.println("  "+p3);
        System.out.println("____________________________________________");
        System.out.println("  " + p2.add(p3));
        
        System.out.println("/////MULTIPLICATION////");
        System.out.println("+ "+p2);
        System.out.println("  "+p3);
        System.out.println("____________________________________________");
        System.out.println("  " + p2.multiply(p3));
    }
}
