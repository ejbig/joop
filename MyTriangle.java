/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class MyTriangle {
    private MyPoint v1;
    private MyPoint v2;
    private MyPoint v3;
    double d1,d2,d3;
    
    
    public MyTriangle() {
        v1 = new MyPoint();
        v2 = new MyPoint();
        v3 = new MyPoint();
    }
    
    public MyTriangle(MyPoint p1, MyPoint p2, MyPoint p3) {
        v1 = new MyPoint(p1.x,p1.y);
        v2 = new MyPoint(p2.x,p2.y);
        v3 = new MyPoint(p3.x,p3.y);
    }
    
    public MyTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        v1 = new MyPoint(x1 , y1);
        v2 = new MyPoint(x2 , y2);
        v3 = new MyPoint(x3 , y3);
    }
    
    public double getPerimeter(MyPoint p1, MyPoint p2, MyPoint p3) {
        v1 = new MyPoint(p1.x,p1.y);
        v2 = new MyPoint(p2.x,p2.y);
        v3 = new MyPoint(p3.x,p3.y);
        d1 = p1.distance(p2);
        d2 = p2.distance(p3);
        d3 = p3.distance(p1);
        return d1 + d2 + d3;
    }
    
    public String printType(MyPoint p1, MyPoint p2, MyPoint p3) {
        //isoceles scalene
        v1 = new MyPoint(p1.x,p1.y);
        v2 = new MyPoint(p2.x,p2.y);
        v3 = new MyPoint(p3.x,p3.y);
        d1 = p1.distance(p2);
        d2 = p2.distance(p3);
        d3 = p3.distance(p1);
        if(d1==d2 && d1==d3 && d2==d3) {
            return "Equillateral";
        } 
        if(d1!=d2 && d2==d3) {
            return "Isoceles";
        }
        if(d1==d3 && d1!=d2 && d3!=d2) {
            return "Isoceles";
        }
        return "Scalene";
        
    }
    
    
    public String toString() {
        return "Triangle @" + v1 + "," + v2 + "," + v3;
    }
}
    
