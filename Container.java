/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class Container {
    private int x1;
    private int x2;
    private int y1;
    private int y2;
    
    public Container(int x, int y, int width, int height) {
       this.x1 = x;
       this.y1 = y;
       this.x2 = width;
       this.y2 = height;
    }
    
    public void setX(int x) {
        this.x1 = x;
    }
    
    public void setY(int y) {
        this.y1 = y;
    }
    
    public void setWidth(int width) {
        this.x2 = width;
    }
    
    public void setHeight(int height) {
        this.y2 = height;
    }
    
    public int getX() {
        return x1;
    }   
    
    public int getY() {
        return y1;
    }
    
    public int getWidth() {
        return x2;
    }
    
    public int getHeight() {
        return y2;
    }
    
    public boolean collidesWith(Ball ball) {
        if(ball.getX() - ball.getRadius() <= this.x1 ||
           ball.getX() - ball.getRadius() >= this.x2) {
           ball.reflectHorizontal();
           return true;
        } else if(ball.getY() - ball.getRadius() <= this.y1||
           ball.getY() - ball.getRadius() >= this.y2) {
           ball.reflectVertical();
           return true;
        }
        return false;
    }
    
    public String toString() {
        return "Container at ("+ this.x1 +","+ this.y1 +") to ("+ this.x2 +","+ this.y2 +")";
    }
    
    
}
