/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class TheAuthorAndBook {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // TODO code application logic here
        Author[] anAuthor = new Author[2];
        
        Book aBook = new Book("Java For Dummy", 19.95, 1000);
        System.out.println(aBook);
        System.out.println("The authors before delete: ");
        aBook.addAuthor(new Author("William Shakespear", "RomeoxJuliet@Poison.com",'m'));
        aBook.addAuthor(new Author("John Green", "GusxHazel@Cancer.com",'m'));
        aBook.printAuthor();
        System.out.println("The authors after delete : ");
        aBook.removeAuthorByName("John Green");
        aBook.printAuthor();
        
    }
    
}
