/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class TestMyTriangle {
    public static void main(String[] args) {
        MyTriangle t1 = new MyTriangle();
        System.out.println(t1);
        //My Parameters for this Excercise(x,y) (x,y) (x,y)
        MyTriangle t2 = new MyTriangle(0,0,0,5,5,0);
        System.out.println(t2);
        //3 points
        MyPoint p1 = new MyPoint(467,51111);
        MyPoint p2 = new MyPoint(2,345);
        MyPoint p3 = new MyPoint(56,499);
        MyTriangle t3 = new MyTriangle(p1,p2,p3);
        System.out.println(p1.distance(p2));
        System.out.println(p1.distance(p3));
        System.out.println(p2.distance(p3));
        System.out.println("PERIMETER: " + t3.getPerimeter(p1, p2, p3));
        System.out.println("TRIANGLE TYPE: " + t3.printType(p1, p2, p3));
    }
}
