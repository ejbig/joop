/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package e212;

/**
 *
 * @author biglangawa
 */
public class Square extends Rectangle {
    public Square () {
        length = 1.0;
        width = 1.0;
    }
    public Square(double side) {
        super(side, side);
    }
    
    public Square(double side, String color, boolean filled) {
        super(side,side,color,filled);
        
    }
    
    public double getSide() {
        return length;
    }
    
    public void setSide(double side) {
        length = side;
    }  
    
    @Override
    public void setWidth(double side) {
        width = side;
    }
    
    @Override
    public void setLength(double side) {
        length = side;
    }
    
    @Override
    public String toString() {
        return "A Square with side-length=" + length + " which is a subclass of " + super.toString();
    }
    
    
    
    
    
    
}
