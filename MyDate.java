/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class MyDate {
    private int year;
    private int month;
    private int day;
    private String strMonths[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private String strDays[] = {"Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
    private int dayInMonths[] = {31,28,31,30,31,30,31,31,30,31,30,31};
    
    public MyDate (int year, int month, int day) {
        setDate(year, month, day);
        
    }
    
    void setDate(int year, int month, int day) {
        setYear(year);
        setMonth(month);
        setDay(day);
    }
    
    void setYear(int year) {
        if(year >= 1 && year <= 9999) {
            this.year = year;
        } else {
            throw new IllegalArgumentException("Invalid Year! Input: 1-9999");
        }
    }
    
    void setMonth(int month) {
        if(month >=1 && month <= 12) {
                this.month = month;
        } else {
            throw new IllegalArgumentException("Invalid Month! Input: 1-12");
        }
    }
    
    void setDay(int day) {
        int dayMax = 0;
        if(year >= 1 && year <= 9999) {
            if(month >=1 && month <= 12) {
                if(this.isLeapYear(year)) {
                    dayMax = dayInMonths[month-1];
                    dayMax += 1;
                } else {
                    dayMax = dayInMonths[month-1];
                }
                if(day >= 1 && day <= dayMax){
                    this.day = day;
                } else {
                    throw new IllegalArgumentException("Invalid Day! Input: 1-"+ dayMax);
                }
            } else {
                throw new IllegalArgumentException("Invalid Month! Input: 1-12");
            }
        } else {
            throw new IllegalArgumentException("Invalid Year! Input: 1-9999");
        }
    }
    
    public int getYear() {
        return year;
    }
    
    public int getMonth() {
        return month;
    }
    
    public int getDay() {
        return day;
    }
    
    public int getDayMax() {
        int dayMax = 0;
        if(year >= 1 && year <= 9999) {
            if(month >=1 && month <= 12) {
                if(this.isLeapYear(year) == true && month==2) {
                    dayMax = dayInMonths[month-1];
                    dayMax += 1;
                } else {
                    dayMax = dayInMonths[month-1];
                }
            } 
        }
        return dayMax;
    }
    
    public String toString() {
        int dayOfWeek = this.getDayOfWeek(year, month, day);
        String strDay;
        strDay = strDays[dayOfWeek];
        return strDay + " DAY: " + day + " " + strMonths[month-1] + " " + year + " ";
    }
    
    public boolean isLeapYear(int year) {
        int leap = year % 4;
        return (leap == 0);
    }
    
    public boolean isValidDate(int year, int month, int day) {
        int dayMax = 0;
        if(year >= 1 && year <= 9999) {
            if(month >=1 && month <= 12) {
                if(this.isLeapYear(year)) {
                    dayMax = dayInMonths[month-1];
                    dayMax += 1;
                } else {
                    dayMax = dayInMonths[month-1];
                }
                if(day >= 1 && day <= dayMax){
                    return true;
                } else {
                    throw new IllegalArgumentException("Invalid Month! Input: 1-"+ dayMax);
                }
            } else {
                throw new IllegalArgumentException("Invalid Month! Input: 1-12");
            }
        } else {
            throw new IllegalArgumentException("Invalid Year! Input: 1-9999");
        }
    }
    
    public int getDayOfWeek (int year, int month, int day) {
        int dayOfWeek = (day +(int)((26 * (month + 1)) / 10.0) +
         year +
         (int)(year / 4.0) +
         6 * (int)(year / 100.0) +
         (int)(year / 400.0))
         % 7;
        //source: wikipedia :3 hehe
        
        return dayOfWeek;
    }
    
    public MyDate nextDay() {
        int dayMax = this.getDayMax();
        day += 1;
        if(day > dayMax) {
            month += 1;
            day = 1;
            if(month >= 12) {
                year += 1;
                month = 1;
            }
        }

        if(year== 9999) {
            year = 1;
            month = 1;
            day = 1;
        }
        return this;
    }
    
    public MyDate nextMonth() {
        int dayMax = this.getDayMax();
        if(day >= dayMax) {
            month += 1;
            dayMax = this.getDayMax();
            day = dayMax;
        } else {
            month += 1;
        }
        
        if(month >= 12 && day > dayMax) {
            year += 1;
            month = 1;
        }
        return this;
    }
    
    public MyDate nextYear() {
        year +=1;
        if(this.isLeapYear(year)==true && month==2) {
           day = 29;
        } else {
           day = 28;
        }
        return this;
    }
    
    public MyDate previousDay() {
        day -= 1;
        int dayMax = this.getDayMax();
        if(day < 1) {
            month -= 1;
            day = dayMax;
        }        
        if(month == 0) {
            month = 12;
            year -=1;
        }
        return this;
    }
        
    public MyDate previousMonth() {
        int dayMax = this.getDayMax();
        if(day == dayMax) {
            month -= 1;
            dayMax = this.getDayMax();
            day = dayMax;
        } else {
            month -= 1;
        }
        
        if(month == 0) {
            month = 12;
        }
        return this;
    }
    
    public MyDate previousYear() {
        year -=1;
        if(this.isLeapYear(year)==true && month==2) {
           day = 29;
        } else {
           day = 28;
        }
        return this;
    }
    
    
}
