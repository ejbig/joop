/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class MyTime {
    private int hour;
    private int minute;
    private int second;
    
    public MyTime(int hour, int minute, int second) {
        setTime(hour, minute, second);
    }
    
    void setTime(int hour, int minute, int second) {
        setHour(hour);
        setMinute(minute);
        setSecond(second);
    }
    
    void setHour(int hour) {
        if(hour >=0 && hour <= 23) {
            this.hour = hour;
        } else {
            throw new IllegalArgumentException("Invalid Hour! Input: 0-23");
        }
    }
    
    void setMinute(int minute) {
        if(minute >=0 && minute <= 59) {
            this.minute = minute;
        } else {
            throw new IllegalArgumentException("Invalid Minute! Input: 0-59");
        }
    }
    
    void setSecond(int second) {
        if(second >=0 && second <= 59) {
            this.second = second;
        } else {
            throw new IllegalArgumentException("Invalid Second! Input: 0-59");
        }
    }
    
    public int getHour() {
        return hour;
    }
    
    public int getMinute() {
        return minute;
    }
    
    public int getSecond() {
        return second;
    }
    
    public String toString() {
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }
    
    public MyTime nextSecond() {
        second +=1;
        if(second==60) {
            minute += 1;
            second = 0;
            
        } 
        if(minute==60) {
            hour += 1;
            minute = 0;
            
        }
        if(hour==24) {
            hour = 0;
            minute = 0;
            second = 0;
        }
        return this;
    }
    public MyTime nextMinute() {
        minute += 1;
        if(minute==60) {
            minute = 0;
            hour += 1;
        }
        if(hour==24) {
            minute = 0;
            hour += 1;
        }
        return this;
    }
    public MyTime nextHour () {
        hour +=1;
        if(hour==24) {
            hour = 0;
            minute = 0;
            second = 0;
        }
        return this;
    }
    
    public MyTime previousSecond() {
        second -=1;
        if(second==0) {
            second = 59;
        } 
        if(minute==0) {
            minute = 59;
        }
        if(hour==0) {
            hour = 23;
            minute = 59;
            second = 59;
        }
        return this;
    }
    public MyTime previousMinute() {
        minute -= 1;
        if(minute==0) {
            minute = 59;
        }
        if(hour==0) {
            hour = 23;
            minute = 59;
            second = 59;
        }
        return this;
    }
    public MyTime previousHour () {
        hour -=1;
        if(hour==0) {
            hour = 23;
            minute = 59;
            second = 59;
        }
        return this;
    }
   
    
    
    
}
