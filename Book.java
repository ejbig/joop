/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

import java.util.Arrays;

/**
 *
 * @author biglangawa
 */
public class Book {
    private String name;
    private Author[] author = new Author[2];
    private int numAuthors = 0;
    private double price;
    private int qtyInStock;
    
    public Book (String name, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }
   
    public Book (String name, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    
    public String getName() {
        return name;
    }
    
    public Author[] getAuthor() {
        return author;
    }
    
    public double getPrice() {
        return price;
    }
    
    public int getQtyInStock() {
        return qtyInStock;
    }
    
    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public String toString() {
        return "Title: " + name + " Price: " + price + " Qty: " + qtyInStock;  
    }
    
    public String[] printAuthor() {
        String result[] = new String[]{"a","b"};
        for(int i = 0; i < author.length; i++) {
          System.out.println(author[i]);
        }
        return result;
     }
    
    public boolean removeAuthorByName(String author) {
        for(int i=0; i < this.author.length; i++) {
            if(this.author[i].name.equals(author)) {
                this.author[i] = null;
                return true;
            }
        }
        return false;
    }
    
    public void addAuthor(Author author) {
        this.author[numAuthors] = author;
        numAuthors += 1;

    }
    
    
    
    
}
