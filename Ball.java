/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class Ball {
    private float x;
    private float y;
    private int radius;
    private float xDelta;
    private float yDelta;
    
    public Ball(int x, int y, int radius, int speed, int direction) {
        this.y = y;
        this.x = x;
        this.radius = radius;
        this.xDelta = speed;
        this.yDelta = direction;
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }
    
    public int getRadius() {
        return radius;
    }
    
    public float getSpeed() {
        return xDelta;
    }
    
    public float getDirection() {
        return yDelta;
    }
    
    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void setX(int x) {
        this.x = x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public void setRadius(int radius) {
        this.radius = radius;
    }
    
    public void setSpeed(float x) {
        this.xDelta = x;
    }
    
    public void setDirection(float y) {
        this.yDelta = y;
    }
        
    public void move() {
        x += xDelta;
        y += yDelta;
    }
    
    public void reflectHorizontal() {
        this.xDelta -= xDelta;
     }
    
    public void reflectVertical() {
        this.yDelta -= yDelta;
    }
    
    public String toString() {
        return "Ball at ("+ this.x +","+ this.y +") with speed of "+ this.xDelta +" and direction of "+ this.yDelta;
    }
    
    
    
}
