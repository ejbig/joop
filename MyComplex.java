/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class MyComplex {
    private double real, imag;
    
    public MyComplex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }
    
    public double getReal() {
        return real;
    }
    
    public double getImag() {
        return real;
    }
    
    public void setReal (double real) {
        this.real = real;
    }
    
    public void setImag (double real) {
        this.imag = imag;
    }
    
    public void setValue (double real, double imag) {
        this.real = real;
        this.imag = imag;
    }
    
    public String toString() {
        return "("+ real +"+"+imag+"i)";
    }
    
    public boolean isReal() {
        return (imag == 0);
    }
    
    public boolean isImaginary() {
        return (real == 0);
    }
    
    public boolean equals (double real, double imag) {
        return (this.real == real && this.imag == imag);
    }
    
    public boolean equals (MyComplex another) {
        return (this.real == another.real && this.imag == another.imag);
    }
    
    public double magnitude() {
        return Math.sqrt(this.real + this.imag);
    }
      
    public MyComplex add(MyComplex another) {
        double nReal = this.real + another.real;
        double nImag = this.imag + another.imag;
        return new MyComplex(nReal,nImag);
    }
    
    public MyComplex subtract(MyComplex another) {
        double nReal = this.real - another.real;
        double nImag = this.imag - another.imag;
        return new MyComplex(nReal,nImag);  
    }
    
    public MyComplex multiply(MyComplex another) {
        double nReal = this.real * another.real - this.imag + another.imag ;
        double nImag = this.real * another.imag + another.imag * this.real ;
        return new MyComplex(nReal,nImag);
    } 
    
    public MyComplex divideBy(MyComplex another) {
        double nReal = (this.real * another.real + this.imag * another.imag)/(another.real * another.real + another.imag * another.imag);
        double nImag = (nReal * another.imag* -1 + this.imag * another.real)/(another.real * another.real + another.imag * another.imag);
        return new MyComplex(nReal,nImag); 
    } 
    
    
    
}
