/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package e212;

/**
 *
 * @author biglangawa
 */
public class TestShape {
    public static void main(String[] args) {
        System.out.println("S#1");
        Shape s1 = new Shape();
        System.out.println(s1);
        
        System.out.println("S#2");
        Shape s2 = new Shape("Pink", false);
        System.out.println(s2);
        
        System.out.println("C#1");
        Circle c1 = new Circle();
        System.out.println("Circle:"
                + " radius = " + c1.getRadius()
                + " perimeter = " + c1.getPerimeter()
                + " base area = " + c1.getArea()
        );
        System.out.println(c1.toString());
        
        System.out.println("C#2");
        Circle c2 = new Circle();
        System.out.println("Circle:"
                + " radius = " + c2.getRadius()
                + " perimeter = " + c2.getPerimeter()
                + " base area = " + c2.getArea()
        );
        System.out.println(c2.toString());
        
        System.out.println("R#1");
        Rectangle r1 = new Rectangle();
        System.out.println("Rectangle:"
                + " width = " + r1.getWidth()
                + " length = " + r1.getLength()
                + " area = " + r1.getArea()
                + " perimeter = " + r1.getPerimeter());
        System.out.println(r1.toString());
        
        System.out.println("R#2");
        Rectangle r2 = new Rectangle(2.0,2.0);
        System.out.println("Rectangle:"
                + " width = " + r2.getWidth()
                + " length = " + r2.getLength()
                + " area = " + r2.getArea()
                + " perimeter = " + r2.getPerimeter());
        System.out.println(r2.toString());
        
        System.out.println("R#3");
        Rectangle r3 = new Rectangle(2.0,2.0,"Maroon",false);
        System.out.println("Rectangle:"
                + " width = " + r3.getWidth()
                + " length = " + r3.getLength()
                + " area = " + r3.getArea()
                + " perimeter = " + r3.getPerimeter());
        System.out.println(r3.toString());
        
        System.out.println("SQ#1");
        Square q1 = new Square();
        System.out.println("Square:"
                + " width = " + q1.getWidth()
                + " length = " + q1.getLength()
                + " area = " + q1.getArea()
                + " perimeter = " + q1.getPerimeter());
        System.out.println(q1.toString());
        
        System.out.println("SQ#2");
        Square q2 = new Square (2.0);
        System.out.println("Square:"
                + " width = " + q2.getWidth()
                + " length = " + q2.getLength()
                + " area = " + q2.getArea()
                + " perimeter = " + q2.getPerimeter());
        System.out.println(q2.toString());
        
        System.out.println("SQ#3");
        Square  q3 = new Square (2.0,"Sky Blue",true);
        System.out.println("Square:"
                + " width = " + q3.getWidth()
                + " length = " + q3.getLength()
                + " area = " + q3.getArea()
                + " perimeter = " + q3.getPerimeter());
        System.out.println(q3.toString());
    }
}
