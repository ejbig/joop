/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class TestMyCircle {
    public static void main(String[] args) {
        MyCircle c1 = new MyCircle();
        System.out.println(c1);      
        c1.setCenterXY(2, 5);
        System.out.println("CENTER SET BY XY: " + c1);      
        MyPoint p1 = new MyPoint (59, 23);
        MyCircle c2 = new MyCircle(p1, 25);      
        System.out.println("CENTER SET BY MyPoint Class: " + c2);
        System.out.println("AREA: "+ c2.getArea());
    }   
}
