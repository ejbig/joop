/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package circle.oop;

/**
 *
 * @author biglangawa
 */
public class CircleOOP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Circle c1 = new Circle();
        System.out.println(c1.toString());
        
        Circle c2 = new Circle(1.2, "Blue");
        System.out.println(c2.toString());
        System.out.println(c2);
        System.out.println("Operator '+' invokes toString() too: " + c2);
        
        Circle c3 = new Circle();
        c3.setRadius(5.0);
        c3.setColor("Pink");
        
    }   
    
}
