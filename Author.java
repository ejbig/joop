/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
public class Author {
    String name;
    private String email;
    private char gender;
    
    public Author () {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }
    
    public Author (String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }
    
    public String getName () {
        return name;
    }
    
    public String getEmail() {
        return email;
    }
    
    public char getGender() { 
        return gender;
    }
    
    public String toString () {
        return name + "("+ gender +") at " + email; 
    }
    public void setEmail(String email) {
        this.email = email;
    }
    
    
}