/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package e212;

/**
 *
 * @author biglangawa
 */
class Shape {
    String color = "red";
    boolean filled = true;
    
    public Shape() {
        this.color = "Green";      
        this.filled = true;
    }
    
    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }
    
    public String getColor() {
        return color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    
    public boolean isFilled(boolean filled) {
        //TEMPORARY UNTIL KNOW WHAT TO DO
        return (filled==true);
    }
    
    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    
    public String toString() {
        if(this.isFilled(this.filled)) {
            return "A shape with color of " + color + " and filled"; 
        } 
        return "A shape with color of " + color + " and not filled"; 
    }
    
    
    
}
