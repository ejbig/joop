/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypoint;

/**
 *
 * @author biglangawa
 */
import	java.util.Scanner;	
public class TestMyComplex {
    public static void main(String args[]) {
        double r1,im1,r2,im2;
        System.out.println("Enter complext number 1(Real and Imaginary part):");
        Scanner s1 = new Scanner(System.in);
        r1 = s1.nextDouble();
        im1 = s1.nextDouble();
        
        System.out.println("Enter complext number 2(Real and Imaginary part):");
        Scanner s2 = new Scanner(System.in);
        r2 = s2.nextDouble();
        im2 = s2.nextDouble();
                
        MyComplex c1 = new MyComplex(r1,im1);
        System.out.println("Number 1 is:" + c1);
        
        if(c1.isReal()) {
            System.out.println(c1 + "is a pure real number");;
        } else {
            System.out.println(c1 + "is NOT a pure real number");
        }
        
        if(c1.isImaginary()) {
            System.out.println(c1 + "is a pure imaginary number");;
        } else {
            System.out.println(c1 + "is NOT a pure imaginary number");
        }
        
        MyComplex c2 = new MyComplex(r2,im2);
        System.out.println("Number 2 is:" + c2);
        
        if(c2.isReal()) {
            System.out.println(c2 + "is a PURE real number");;
        } else {
            System.out.println(c2 + "is NOT a pure real number");
        }
        
        if(c2.isImaginary()) {
            System.out.println(c2 + "is a PURE imaginary number");
        } else {
            System.out.println(c2 + "is NOT a pure imaginary number");
        }
        
        if(c1.equals(c2)) {
            System.out.println(c1 + "is EQUALS to" + c2);
        } else {
            System.out.println(c1 + "is NOT equal to" + c2);
        }
        
        System.out.println(c1 + "+" + c2 + " = " + c1.add(c2));
        System.out.println(c1 + "-" + c2 + " = " + c1.subtract(c2));
        System.out.println(c1 + "*" + c2 + " = " + c1.multiply(c2));
        System.out.println(c1 + "/" + c2 + " = " + c1.divideBy(c2));
        
        
    }    
}
