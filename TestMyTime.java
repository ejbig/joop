
package mypoint;

/**
 *
 * @author biglangawa
 */
public class TestMyTime {
    public static void main (String args[]) {
        /*
        OUTPUT:
        22:58:58
        22:58:59
        22:59:59
        23:59:59
        00:00:00
        23:59:59
        23:58:59
        22:58:59
        22:58:58
        22:58:59
        22:59:00
         */
        MyTime t1 = new MyTime(22, 58, 58);
        System.out.println(t1);
        System.out.println(t1.nextSecond());
        System.out.println(t1.nextMinute());
        System.out.println(t1.nextHour());
        System.out.println(t1.nextSecond());
        System.out.println(t1.previousSecond());
        System.out.println(t1.previousMinute());
        System.out.println(t1.previousHour());
        System.out.println(t1.previousSecond());
    }
}
