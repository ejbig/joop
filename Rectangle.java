/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package e212;

/**
 *
 * @author biglangawa
 */
public class Rectangle extends Shape{
    double width = 1.0;
    double length = 1.0;
    
    public Rectangle() {
        width = 1.0;
        length = 1.0;
    }
    
    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }
    
    public Rectangle(double width, double length, String color, boolean filled) {
        super(color,filled);
        this.width = width;
        this.length = length;
    }
    
    public double getWidth() {
        return width;
    }
    
    public void setWidth(double width) {
        this.width = width;
    }
    
    public double getLength() {
        return length;
    }
    
    public void setLength(double length) {
        this.length = length;
    }
    
    public double getArea() {
        return length*width;
    }
    
    public double getPerimeter() {
         return length + length + width + width;
    }
    
    @Override
    public String toString() {
        return "A Rectangle with length=" + length + " and width= "+ width +" which is a subclass of " + super.toString();
    }
    
    
    
    
}

